#include <stdio.h>
#include<string.h>
int lower_string(char []);

int main()
{
   char string[100];
   
   printf("Enter a string to convert it into lower case\n");
   gets(string);
   
   lower_string(string);
   
   printf("The string in lower case: %s\n", string);
     
   return 0;
}

int lower_string(char s[]) {
   int c = 0;
   
   while (s[c] != '\0') {
      if (s[c] >= 'A' && s[c] <= 'Z') {
         s[c] = s[c] + 32;
      }
      c++;
   }
   return 0;
}