#include <stdio.h>
#include <string.h>

int Palindrome(char str[]);
 
int main()
{
  	char str[100];
 
  	printf("\n Please Enter any String :  ");
  	gets(str);
  	
  	Palindrome(str);	
	
  	return 0;
}

int Palindrome(char str[])
{
	int i = 0,j;
	int len=0; 
	for(j=0;str[j]!='\0';j++)
	{
		len++;
	}
	len=len-1;
	while (len > i)
	{
		if(str[i++] != str[len--])
		{
			printf("\n %s is Not a Palindrome String", str);
			return 0;
		}
	}
	printf("\n %s is a Palindrome ", str);
}
